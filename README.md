# Active window resource management

This extension is meant to be used as an experimental demo.

Function/Use: It tracks the active window and increases the resource allocated to its cgroup.

Needs `uresourced` to work properly. 

This is a repository created for work related GSoC 2021.
