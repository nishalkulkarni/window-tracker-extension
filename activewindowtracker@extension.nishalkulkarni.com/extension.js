const { Clutter, Gio, GLib, GObject, Meta, Shell, St } = imports.gi;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;

const Mainloop = imports.mainloop;

class Extension {
    constructor() {
        this._indicator = null;

        this._windowFocusChangedId = 0;
        this._previous_pid = 0;

        this.settings = ExtensionUtils.getSettings(
            'org.gnome.shell.extensions.activewindowtracker');
    }

    _setActiveWindow(_window_pid, _active_window = true) {
        try {
            let [ok, contents] = GLib.file_get_contents(`/proc/${_window_pid}/cgroup`);

            if (!ok) {
                log("Can't read cgroup file");
                return false;
            }

            // log("Complete Path: ", String(contents).trim());

            contents = String(contents).split("/");
            let userServiceIndex = contents.findIndex(val => /user@.*\.service/.test(val));
            var app_unit_name = null;
            var app_unit_index = 0;

            for (let i = userServiceIndex + 1; i < contents.length; i++) {
                if (!contents[i].endsWith(".slice")) {
                    app_unit_name = contents[i].trim();
                    app_unit_index = i;
                    break;
                }
            }

            if (!app_unit_name) {
                log("Can't find app unit name");
                return false;
            }

            log("Unit name: " + app_unit_name);

            try {
                var path = ["/sys/fs/cgroup"];
                for (var i = 1; i < app_unit_index + 1; i++) {
                    path.push(String(contents[i]).trim());
                }
                path = GLib.build_filenamev(path);

                var file = Gio.file_new_for_path(path);
                var fileInfo = new Gio.FileInfo();

                var timestamp = (_active_window) ? -1 : GLib.get_monotonic_time();

                fileInfo.set_attribute_string("xattr::xdg.inactive-since", String(timestamp));

                var attrSet = file.set_attributes_from_info(fileInfo,
                    Gio.FileQueryInfoFlags.NONE,
                    null);

                if (!attrSet) {
                    log(`Error setting xattr for ${app_unit_name}`);
                }
            } catch (e) {
                if (e instanceof Gio.DBusError) {
                    let errorName = e.get_remote_error();
                    Gio.DBusError.strip_remote_error(e);
                }
                logError(e);
            }

            return true;
        } catch (error) {
            logError(error);
            return false;
        }
    }

    _onFocusChanged() {
        var active_window = global.display.get_focus_window();
        if (active_window != undefined) {
            let active_window_name = active_window.get_wm_class();
            let active_window_pid = active_window.get_pid();

            log(`Active window: Name: ${active_window_name} PID: ${active_window_pid}`);

            if (this._previous_pid != 0) {
                if (!this._setActiveWindow(this._previous_pid, false)) {
                    log(`Error: Previous CGroup resources not reset`);
                    this._previous_pid = 0;
                }
            }

            if (this._setActiveWindow(active_window_pid, true)) {
                log(`xattr for ${active_window_name} set successfully.`);
                this._previous_pid = active_window_pid;
            } else {
                log(`Error: xattr for ${active_window_name} not set.`);
                this._previous_pid = 0;
            }
        } else {
            log(`No active window found`);
        }
    }

    enable() {
        log(`enabling ${Me.metadata.name}`);

        let indicatorName = `${Me.metadata.name} Indicator`;

        // Create a panel button
        this._indicator = new PanelMenu.Button(0.0, indicatorName, false);

        // Add an icon
        let icon = new St.Icon({
            gicon: new Gio.ThemedIcon({ name: 'face-laugh-symbolic' }),
            style_class: 'system-status-icon'
        });
        this._indicator.add_child(icon);

        // Bind our indicator visibility to the GSettings value
        this.settings.bind(
            'show-indicator',
            this._indicator,
            'visible',
            Gio.SettingsBindFlags.DEFAULTlg
        );

        Main.panel.addToStatusArea(indicatorName, this._indicator);

        this._windowFocusChangedId =
            global.display.connect('notify::focus-window',
                this._onFocusChanged.bind(this));
    }

    disable() {
        log(`disabling ${Me.metadata.name}`);

        global.display.disconnect(this._windowFocusChangedId);
        this._windowFocusChangedId = 0;

        this._indicator.destroy();
        this._indicator = null;
    }
}


function init() {
    log(`initializing ${Me.metadata.name}`);

    return new Extension();
}
